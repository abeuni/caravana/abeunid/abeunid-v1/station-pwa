import '@firebase/firestore'
import firebase from '@firebase/app'

import uuid from 'uuid/v4'
import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const db = firebase
  .initializeApp({
    apiKey: 'AIzaSyA1ecjbVy8vFNhy9AWgEAFQlG41WLpn8eQ',
    authDomain: 'caravana-system.firebaseapp.com',
    databaseURL: 'https://caravana-system.firebaseio.com',
    projectId: 'caravana-system',
    storageBucket: 'caravana-system.appspot.com',
    messagingSenderId: '236368191983',
    appId: '1:236368191983:web:3e9c8004cc77afa3'
  })
  .firestore()

export default new Vuex.Store({
  plugins: [
    createPersistedState({
      reducer: persistedState => {
        const stateFilter = Object.assign({}, persistedState)
        const blackList = ['CEPData']

        blackList.forEach(item => {
          delete stateFilter[item]
        })

        return stateFilter
      }
    })
  ],
  state: {
    checkpoints: []
  },
  getters: {
    checkpoints (state) {
      return state.checkpoints
    }
  },
  mutations: {
    addCheckpoint (state, checkpoint) {
      state.checkpoints.unshift({
        metadata: {
          uploaded: false,
          origin: 'local'
        },
        data: {
          id: uuid(),
          code: checkpoint.code,
          place: checkpoint.place,
          timestamp: new Date().toISOString()
        }
      })
      console.log(state.checkpoints[0].data.id)
    },
    uploadedCheckpoint (state, index) {
      state.checkpoints[index].metadata.uploaded = true
    },
    downloadedAssignment (state, data) {
      state.checkpoints.unshift({ metadata: { origin: 'remote' }, data })
    }
  },
  actions: {
    async sync ({ state, commit }) {
      const checkpoints = db.collection('checkpoints')

      // Local cached data
      const cached = state.checkpoints.filter(entry => !entry.metadata.uploaded)

      await Promise.all(
        cached.map((entry, index) =>
          checkpoints
            .doc(entry.data.id)
            .set(entry.data)
            .then(() => {
              commit('uploadedCheckpoint', index)
            })
        )
      )

      // Don't fetch checkpoints already in local
      const checkpointsLocal = state.checkpoints.map(entry => entry.data.id)
      const checkpointsSnapshot = await checkpoints.get()

      checkpointsSnapshot.docs.forEach(doc => {
        if (!checkpointsLocal.includes(doc.id)) {
          const data = doc.data()
          commit('downloadedCheckpoint', data)
        }
      })
    }
  }
})
